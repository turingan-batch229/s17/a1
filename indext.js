/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:

function printWelcomeMessage(){
	let fullName = prompt("Enter your Full Name");
	let Age = prompt("Enter your Age");
	let Location = prompt("Enter your location");


	console.log("Hello, " + fullName + "!");
	console.log("You are " + Age + " years old.")
	console.log("You live in " + Location + ".")
	
}

printWelcomeMessage();


	

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function printBand(){
	var functionVar = ("1. " + "Parokya ni Edgar.");
	const functionConst = ("2. " + "Freestyle.");
	let functionLet = ("3. " + "Side A");
	let functionLet2 = ("4. " + "Counting Crows");
	let functionLet3 = ("5. " + "Sugar Ray");

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
	console.log(functionLet2);
	console.log(functionLet3);
}

printBand();



/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function faveMovies(){
	
	let movie = ["Avengers", "The Wedding Singer", "Avengers Infinity War", "Avengers Endgame", "The Dark Knight"]
	let movieRate = ["91%", "69%", "85%", "94%", "94%"]

	for(let fav = 0; fav < 5; fav++){

	let numbering = fav +1;

	console.log(numbering + ". " + movie[fav]);
	console.log("Rotten Tomatoes Rating: " + movieRate[fav]);
	}
}
faveMovies()


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with: " + printFriends);
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();



// console.log(friend1);
// console.log(friend2);
